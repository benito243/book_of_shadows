var app = (function () {
  // foolBowHead = function(){
  //     foolHead.setAttribute('transform','rotate(10 140 97)');
  // };
  // foolRaiseHead = function(){
  //     foolHead.setAttribute('transform','rotate(0 140 97)');
  // };
  //
  // mageBowHead = function(){
  //     mageHeadFront.setAttribute('transform','rotate(20 124 123)');
  //     mageHeadBack.setAttribute('transform','rotate(20 124 123)');
  // };
  // mageRaiseHead = function(){
  //     mageHeadFront.setAttribute('transform','rotate(0 124 123)');
  //     mageHeadBack.setAttribute('transform','rotate(0 124 123)');
  // };
  //
  // empressBowHead = function(){
  //     empressHeadFront.setAttribute('transform','rotate(-15 146 134)');
  //     //empressHeadBack.setAttribute('transform','rotate(-15 146 134)');
  //     //empressBook.setAttribute('transform','rotate(3 138 284)');
  // };
  // empressRaiseHead = function(){
  //     empressHeadFront.setAttribute('transform','rotate(0 146 134)');
  //     //empressHeadBack.setAttribute('transform','rotate(0 146 134)');
  //     //empressBook.setAttribute('transform','rotate(0 138 284)');
  // };

  getTaroDeck = function () {
    var taroDeckObject = document.querySelectorAll('symbol:not(#card-back)')
    var counter = taroDeckObject.length
    var taroDeck = []
    while (counter--) {
      taroDeck.push(taroDeckObject[counter].id)
    }
    return taroDeck
  }

  shuffleDeck = function (array) {
    let counter = array.length

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      const index = Math.floor(Math.random() * counter)

      // Decrease counter by 1
      counter--

      // And swap the last element with it
      const temp = array[counter]
      array[counter] = array[index]
      array[index] = temp
    }

    return array
  }

  spread = function (taroDeck) {
    var cardsOnScreen = document.querySelectorAll('.container .front use')
    var counter = cardsOnScreen.length

    while (counter--) {
      cardsOnScreen[counter].setAttributeNS('http://www.w3.org/1999/xlink', 'href', '#' + taroDeck[counter])
    }
  }

  setAnimations = function () {
    var foolCard = document.querySelector('#fool-card').parentNode.parentNode.parentNode.parentNode
    var fool = document.querySelector('#fool')
    var foolHead = fool.querySelector('.head')

    var mageCard = document.querySelector('#mage-card').parentNode.parentNode.parentNode.parentNode
    var mage = document.querySelector('#mage')
    var mageHeadFront = mage.querySelector('.head-foreground')
    var mageHeadBack = mage.querySelector('.head-background')

    var empressCard = document.querySelector('#empress-card').parentNode.parentNode.parentNode.parentNode
    var empress = document.querySelector('#empress')
    var empressHeadFront = empress.querySelector('.head')

    if (foolCard && typeof foolBowHead === 'function') { foolCard.addEventListener('mouseover', foolBowHead, false) }
    if (foolCard && typeof foolRaiseHead === 'function') { foolCard.addEventListener('mouseout', foolRaiseHead, false) }
    if (mageCard && typeof mageBowHead === 'function') { mageCard.addEventListener('mouseover', mageBowHead, false) }
    if (mageCard && typeof mageRaiseHead === 'function') { mageCard.addEventListener('mouseout', mageRaiseHead, false) }
    if (empressCard && typeof empressBowHead === 'function') { empressCard.addEventListener('mouseover', empressBowHead, false) }
    if (empressCard && typeof empressRaiseHead === 'function') { empressCard.addEventListener('mouseout', empressRaiseHead, false) }
  }

  taroDeck = getTaroDeck()
  taroDeck = shuffleDeck(taroDeck)
  spread(taroDeck)
  // setAnimations();
})()
